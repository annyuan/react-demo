var dirs = {
		sourceDir: './web/src/',
		sourceAssetsDir: './web/src/assets/',
		buildDir: './web/build/',
		buildAssetsDir: './web/build/assets/'
	};

module.exports = {
	env:				'WEB',
	port: 5678,
	react: true,
	local_api: 'http://localhost:8080/api', //app is local, api is local
	test_api: 'https://test-environment.appspot.com/api', //app is local, api is test
	build_api: '/api', //app is built for remote, api is remote
	cdn_url: '/', //used when gulp is in build mode
	sourceDir:			dirs.sourceDir,
	sourceAssetsDir:	dirs.sourceAssetsDir,
	buildDir:			dirs.buildDir,
	buildAssetsDir:		dirs.buildAssetsDir,
	sources: {
		markup: {
			watchSource: [
				dirs.sourceAssetsDir + 'index.html',
				dirs.sourceAssetsDir + 'template/**/*.{hbs,html}'
			],
			watchFolder: dirs.sourceAssetsDir + 'template'
		},
		sass: {
			watchSource: dirs.sourceAssetsDir + 'scss/**/*.scss'
		},
		images: {
			watchSource: [
				dirs.sourceAssetsDir + 'img/*'
			]
		},
		browserify: {
			watchSource: [
				dirs.sourceAssetsDir + 'js/**/*',				// all js files...
				'!' + dirs.sourceAssetsDir + 'js/bundle.js',	// ...except for bundled js,
				'!' + dirs.sourceAssetsDir + 'js/lib/*'		// and libraries
			],
			bundleSource: dirs.sourceAssetsDir + 'js/bundle.js',
			entryPoint: dirs.sourceAssetsDir + 'js/main.js'
		}
	},
	targets: {
		markup: {
			bundleFileName: dirs.sourceAssetsDir + 'index.html'
		},
		sass: {
			bundleFileName: 'styles.css',
			orientationBundleFileName: 'orientation-roadblock.css',
			bundleDir: dirs.sourceAssetsDir + 'css/'
		},
		images: {
			bundleDir: dirs.buildAssetsDir + 'img/'
		},
		browserify: {
			bundleFileName: 'bundle.js',
			bundleDir: dirs.sourceAssetsDir + 'js/'
		}
	}
};