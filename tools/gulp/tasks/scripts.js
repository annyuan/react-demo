var gulp = require('gulp'),
	gutil = require('gulp-util'),
	browserify = require('browserify'),
	browserifyHandlebars = require('browserify-handlebars'),
	handlebarsify = require('hbsfy'),
	watchify = require('watchify'),
	streamify = require('gulp-streamify'),
	connect = require('gulp-connect'),
	source = require('vinyl-source-stream'),
	jshint = require('gulp-jshint'),
	config = require('../config'),
	handleErrors = require('../util/handleErrors'),
	react = require('gulp-react'),
	reactify = require('reactify'),
	parseArgs = require('minimist')(process.argv.slice(2)),
	buildMode = (parseArgs['_'][0] === 'build') ? true : false;

gulp.task('js-hint', function() {
	// gutil.log(gutil.colors.yellow('[ JSHint ]'));
	var stream = gulp.src(config.sources.browserify.watchSource);
	
	if(config.react) {
		stream = stream.pipe(react()); //transform any react files before linting
	}

	stream.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish'))
		.on('error', handleErrors);
});

gulp.task('externalScripts', [], function() {
	gulp.src([config.sourceAssetsDir + 'js/lib/**', '!' + config.sourceAssetsDir + 'js/lib/**/*.min.js'])
		.pipe(jshint.reporter('jshint-stylish'))
		.on('error', handleErrors)
		.pipe(gulp.dest(config.buildAssetsDir + 'js/lib/'));
});

gulp.task('scripts', [], function() {
	return doScripts(false);
});

gulp.task('watchScripts', [], function() {
	return doScripts(true);
});

var options = {},
	browserify_instance, bundler = null;

function doScripts(watch) {
	if(bundler === null) {
		options = {
			cache: {},			// required by watchify
			packageCache: {},	// required by watchify
			fullconfig: watch,	// required by watchify
			//
			entries: [config.sources.browserify.entryPoint],
			debug: watch
		};
		browserify_instance = browserify(config.sources.browserify.entryPoint, options);
		if(watch) {
			bundler = watchify(browserify_instance); // watchify instance wrapping a browserify instance
			bundler.on('update', rebundle);
		} else {
			bundler = browserify_instance; // browserify instance
		}
	}

	if(config.react) {
		bundler = bundler.transform(reactify);
	}

	bundler = bundler.transform(handlebarsify);
	
	function rebundle(ids) {
		if(!buildMode && watch) gulp.start('js-hint'); 
		if(ids && watch) gutil.log(gutil.colors.yellow('[ Browserify ] Rebundle: ', ids));
		return bundler.bundle()
			.on('error', function(err) {
				handleErrors.call(this, err);
			})
			.pipe(source(config.targets.browserify.bundleFileName))
			.pipe(gulp.dest(config.targets.browserify.bundleDir))
			.pipe(gulp.dest(config.buildAssetsDir + '/js'));
	}
	return rebundle();
}