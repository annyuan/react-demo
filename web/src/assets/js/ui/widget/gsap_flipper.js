/** @jsx React.DOM */

var Flipper = React.createClass({
	getInitialState: function() {
		return {
			opacity: 1
		};
	},

	componentWillReceiveProps: function() {
		TweenLite.to(this, 0.5, {state: {opacity: 0}});

		setTimeout(function() {
			TweenLite.to(this, 0.5, {state: {opacity: 1}});
		}.bind(this), this.props.transDur * 1000);
	},

	render: function() {
		var style = {};

		style.opacity = this.state.opacity;

		return (
			<div style={style} className="flipper">
				{this.props.frontActive ? "Front" : "Back"} - Click to flip
			</div>
		);
	}
});

module.exports = Flipper;