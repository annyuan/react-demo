/** @jsx React.DOM */
var Indicator = React.createClass({
	render: function() {
		var containerStyle = {
			width: this.props.stepWidth * this.props.stepCount
		},
		translateX = this.props.position * this.props.stepWidth,
		indicatorStyle = {
			width: this.props.stepWidth,
			transform: "translate3d(" + translateX + "px, 0, 0)"
		};

		return (
			<div style={containerStyle} className="indicator_container">
				<div style={indicatorStyle} className="indicator"></div>
			</div>
		);
	}
});

module.exports = Indicator;