/** @jsx React.DOM */

var u = require('../../lib/grow/util.js');

var Card = React.createClass({
	getInitialState: function() {
		return {
			rotationX: 0
		};
	},

	componentWillReceiveProps: function() {
		TweenLite.to(this, this.props.transDur, {
			state: {rotationX: (this.state.rotationX === 0 ? 180 : 0)},
			delay: this.props.index * this.props.transDur
		});
	},

	render: function() {
		var style = {};

		style[u.prefixedProperties.transform.dom] = "rotateX(" + this.state.rotationX + "deg)";

		return (
			<div style={style} className="card">
				<div className="front"></div>
				<div className="back"></div>
			</div>
		);
	}
});

module.exports = Card;