/** @jsx React.DOM */
var Detail = React.createClass({
	stepClicked: function() {
		this.props.stepClicked(this.props.index);
	},

	render: function() {
		var style = {
			width: this.props.width
		};

		return (
			<div style={style} onClick={this.stepClicked} className="step"></div>
		);
	}
});

module.exports = Detail;