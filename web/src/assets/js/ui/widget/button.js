/** @jsx React.DOM */
var Button = React.createClass({
	getDefaultProps: function () {
		return {
			label: '',
			onClick: function() {}
		};
	},

	render: function () {
		return (
			<button className="button" onClick={this.props.onClick}>{this.props.label}</button>
		);
	}
});

module.exports = Button;