/** @jsx React.DOM */
var Detail = React.createClass({
	render: function() {
		return (
			<div data-active={this.props.active} className="detail">
				<div className="heading">{this.props.heading}</div>
				<div className="description">{this.props.description}</div>
			</div>
		);
	}
});

module.exports = Detail;