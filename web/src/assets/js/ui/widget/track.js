/** @jsx React.DOM */
var Step = require('./step'),
	Track = React.createClass({
	stepClicked: function(index) {
		this.props.trackClicked(index);
	},

	render: function() {
		var steps = this.props.steps.map(function(step, stepIndex) {
			return <Step width={this.props.stepWidth} label={stepIndex + 1} key={stepIndex} index={stepIndex} stepClicked={this.stepClicked}/>;
		}.bind(this));

		return (
			<div className="track">
				{steps}
			</div>
		);
	}
});

module.exports = Track;