/** @jsx React.DOM */
var Card = React.createClass({
	render: function() {
		return (
			<div className={"card" + (this.props.frontActive ? "" : " flipped")}>
				<div className="front"></div>
				<div className="back"></div>
			</div>
		);
	}
});

module.exports = Card;