/** @jsx React.DOM */
var Arrow = React.createClass({
	handleClick: function() {
		this.props.arrowClicked(this.props.direction);
	},

	render: function() {
		return (
			<div onClick={this.handleClick} className={((this.props.disabled === true) ? "disabled " : " ") + this.props.direction + " arrow"}></div>
		);
	}
});

module.exports = Arrow;