/** @jsx React.DOM */
var Router = require('react-router'),
	Detail = require('../widget/detail'),
	Track = require('../widget/track'),
	Arrow = require('../widget/arrow'),
	Indicator = require('../widget/indicator'),
	u = require('../../lib/grow/util.js');

var Home = React.createClass({

	getInitialState: function() {
		return {
			activeStepIndex: 0
		};
	},

	getDefaultProps: function() {
		return {
			stepWidth: 40,
			steps: [
				{
					name: "one",
					description: "Choose player."
				},
				{
					name: "two",
					description: "Choose effect."
				},
				{
					name: "three",
					description: "Choose sticker."
				},
				{
					name: "four",
					description: "Choose text."
				},
				{
					name: "five",
					description: "Done."
				}
			]
		};
	},

	arrowClicked: function(direction) {
		var increment = 1;
		if(direction === "previous") {
			increment = -1;
		}

		this.setState({
			activeStepIndex: this.state.activeStepIndex + increment,
			direction: direction
		});
	},

	trackClicked: function(index) {
		this.setState({
			activeStepIndex: index
		});
	},

	render: function () {
		var details = this.props.steps.map(function(step, stepIndex) {
			return <Detail active={stepIndex === this.state.activeStepIndex} key={stepIndex} heading={step.name} description={step.description}/>;
		}.bind(this));

		return (
			<div data-direction={this.state.direction} className="home">
				<div className="highlight">{details}</div>
				<div className="controls">
					<Arrow arrowClicked={this.arrowClicked} disabled={(typeof this.props.steps[this.state.activeStepIndex - 1]) === "undefined" ? true : false} direction="previous"/>
					<Arrow arrowClicked={this.arrowClicked} disabled={(typeof this.props.steps[this.state.activeStepIndex + 1]) === "undefined" ? true : false} direction="next"/>
					{details}
					<Track stepWidth={this.props.stepWidth} trackClicked={this.trackClicked} steps={this.props.steps}/>
					<Indicator stepCount={this.props.steps.length} stepWidth={this.props.stepWidth} position={this.state.activeStepIndex}/>
				</div>
			</div>
		);
	}
});

module.exports = Home;