/** @jsx React.DOM */
var Router = require('react-router'),
	Link = Router.Link,
	gsap_react_plugin = require('gsap-react-plugin'),
	Card = require('../widget/gsap_card'),
	Flipper = require('../widget/gsap_flipper');

var About = React.createClass({

	getInitialState: function() {
		return {
			frontActive: true
		};
	},

	getDefaultProps: function() {
		return {
			totalFlipDur: 2
		};
	},

	flipperClicked: function() {
		this.setState({
			frontActive: !this.state.frontActive
		});
	},

	render: function () {
		var cards = [],
			style = {};

		for(var i=0; i<4; i++) {
			cards.push(<Card index={i} transDur={this.props.totalFlipDur / 4} frontActive={this.state.frontActive}/>);
		}

		style.opacity = this.state.flipperOpacity;

		return (
			<div className="about">
				<h2>About Page!</h2>
				<Link to="/">Go to home</Link>
				<div className="flipper-container" onClick={this.flipperClicked}>
					<Flipper transDur={this.props.totalFlipDur} frontActive={this.state.frontActive} />
				</div>
				<div className="card-container">
					{cards}
				</div>
			</div>
		);
	}
});

module.exports = About;