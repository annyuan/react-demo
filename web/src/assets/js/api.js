/**
 * Handles API calls to the server. Dependency on window.baseApiUrl being set.
 */
var BaseAPI = require('./lib/grow/base_api.js');

function API() {}

API.prototype = new BaseAPI();

API.prototype.exampleGet = function (exampleParam, callback) {
	this.get('example_endpoint/' + exampleParam, callback);
};

API.prototype.examplePost = function (json, callback) {
	this.post('example_endpoint', json, callback);
};

module.exports = new API();