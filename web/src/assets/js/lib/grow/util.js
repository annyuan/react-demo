module.exports = {
	vendors: ['webkit', 'Moz', 'O', 'ms'],
	prefixedProperties: {
		transform: {
			js: "transform",
			dom: "transform"
		}, 
		transformOrigin: {
			js: "transformOrigin",
			dom: "transform-origin"
		},
		animation: {
			js: "animation",
			dom: "animation"
		}
	},
	isEmptyObject: function(obj) {
		return Object.keys(obj).length === 0;
	},
	bindAll: function(obj, functions) {
		functions.forEach(function (fn) {
			obj[fn] = obj[fn].bind(obj);
		});
	},
	degreesToRadians: function(deg) {
		return Math.PI * deg / 180;
	},
	combine: function (left, right) {
		Object.keys(right).forEach(function (key) {
			left[key] = right[key];
		});

		return left;
	},
	extend: function(props) {
		var prop, obj;
        obj = Object.create(this);
        for(prop in props) {
            if(props.hasOwnProperty(prop)) {
                obj[prop] = props[prop];
            }
        }
        return obj;
	},
	arrayEquals: function(a, b) {
		if(a.length !== b.length) {
			return false;
		}

		for(var i=0; i < a.length; i++) {
			if(a[i] !== b[i]) {
				return false;
			}
		}
		return true;
	},
	capitalize: function(str) {
		return str.charAt(0).toUpperCase() + str.slice(1);
	},
	sign: function(x) {
	    x = +x;
	    if (x === 0 || isNaN(x)) {
	        return x;
	    }
	    return x > 0 ? 1 : -1;
	},
	init: function() {
		this.raf = window.webkitRequestAnimationFrame || window.requestAnimationFrame;

		this.vendors.every(function(prefix) {
			var e = prefix + 'Transform';
			if(typeof document.body.style[e] !== 'undefined') {
				Object.keys(this.prefixedProperties).forEach(function(prop, index) {
					this.prefixedProperties[prop].js = prefix + this.capitalize(prop);
					this.prefixedProperties[prop].dom = "-" + prefix + "-" + prop;
				}.bind(this));
				return false;
			}
			return true;
		}.bind(this));
	}
};
