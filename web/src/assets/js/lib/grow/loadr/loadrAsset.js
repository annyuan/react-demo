(function(global) {

	/*
	* # # # # # # # # # # # # # # # # # # # # # #
	*
	* Provides loading functionality for a generic asset.
	* When the asset has been loaded this instance will fire the onLoad function of its
	* parent loadr, or fire the onError function if the loading process errors out in any way.
	*
	* @function loadrAsset
	*
	* # # # # # # # # # # # # # # # # # # # # # #
	*/

	function loadrAsset() {
		
		var self = this;

		/*
		* ====================
		*
		* Reference to parent loadr instance
		*
		* @property loadr
		* @type loadr
		*
		* ====================
		*/

		this.loadr = null;
		
		/*
		* ====================
		*
		* Source dom element
		*
		* @property srcEl
		* @type *
		*
		* ====================
		*/

		this.srcEl = null;

		/*
		* ====================
		*
		* Boolean that tracks whether a halt action was called on this loadr asset instance.
		*
		* @property halted
		* @type Boolean
		*
		* ====================
		*/

		this.halted = false;

		/*
		* ====================
		*
		* Object that contains properties pertaining to halt actions.
		*
		* @property haltedObj
		* @type Object
		*
		* ====================
		*/

		this.haltedObj = {
			callback:	null,	// optional callback for when asset load is halted
			fired:		false	// ensure the halt callback is fired only once
		};

		// # # # # # # # # # # # # # # # # # # # # # #
		//
		// PUBLIC
		//
		// # # # # # # # # # # # # # # # # # # # # # #

		/*
		* ====================
		*
		* Fired when a state change is detected on the source dom element.
		*
		* @function onReadyStateChange
		*
		* ====================
		*/

		this.onReadyStateChange = function() {
			if(self.srcEl.readyState === 'complete') {
				self.removeEventHandlers();
				self.loadr.onLoad(self);
			}
		};

		/*
		* ====================
		*
		* Fired when the load event is detected on the source dom element.
		*
		* @function onLoad
		*
		* ====================
		*/

		this.onLoad = function() {
			self.removeEventHandlers();
			self.loadr.onLoad(self);
		};

		/*
		* ====================
		*
		* Fired when an error is detected on the source dom element.
		*
		* @function onError
		*
		* ====================
		*/

		this.onError = function() {
			self.removeEventHandlers();
			self.loadr.onError(self);
		};

		/*
		* ====================
		*
		* Fired when a timeout is detected on the source dom element.
		*
		* @method onTimeout
		*
		* ====================
		*/

		this.onTimeout = function() {
			self.removeEventHandlers();
			if(self.srcEl.complete) {
				self.loadr.onLoad(self);
			} else {
				self.loadr.onTimeout(self);
			}
		};

		/*
		* ====================
		*
		* Removes all event handlers on the source dom element.
		*
		* @function removeEventHandlers
		*
		* ====================
		*/

		this.removeEventHandlers = function() {
			self.off('load', self.onLoad);
			self.off('readystatechange', self.onReadyStateChange);
			self.off('error', self.onError);
		};

		/*
		* ====================
		*
		* Begins loading process for a single asset.
		*
		* @method start
		* @param p_loadr_instance {loadr} Instance of loadr that this asset will report to
		*
		* ====================
		*/

		this.start = function(p_loadr_instance) {
			/*

			self.loadr = p_loadr_instance;

			self.on('load', self.onLoad);
			self.on('readystatechange', self.onReadyStateChange);
			self.on('error', self.onError);

			*/
		};

		/*
		* ====================
		*
		* Checks the srcEl element's complete state.
		* Useful if the complete event fires before handler is added.
		*
		* @method checkStatus
		*
		* ====================
		*/

		this.checkStatus = function() {
			if (self.srcEl.complete) {
				self.removeEventHandlers();
				self.loadr.onLoad(self);
			}
		};

		// # # # # # # # # # # # # # # # # # # # # # #
		//
		// UTILITY
		//
		// # # # # # # # # # # # # # # # # # # # # # #

		/*
		* ====================
		*
		* Add event handling to the source element.
		*
		* @method on
		* @param p_event_name {String} Name of a valid event to listen to
		* @param p_event_handler {Function} Callback to attach and fire once the event is triggered
		*
		* ====================
		*/

		this.on = function(p_event_name, p_event_handler) {
			if(self.srcEl.addEventListener) {
				self.srcEl.addEventListener(p_event_name, p_event_handler, false);
			} else if(self.srcEl.attachEvent) {
				self.srcEl.attachEvent('on' + p_event_name, p_event_handler);
			}
		};

		/*
		* ====================
		*
		* Remove event handling from the source element.
		*
		* @method off
		* @param p_event_name {String} Name of a valid event to stop listening to
		* @param p_event_handler {Function} Reference to the attached callback that should be removed
		*
		* ====================
		*/

		this.off = function(p_event_name, p_event_handler) {
			if(self.srcEl.removeEventListener) {
				self.srcEl.removeEventListener(p_event_name, p_event_handler, false);
			} else if(self.srcEl.detachEvent) {
				self.srcEl.detachEvent('on' + p_event_name, p_event_handler);
			}
		};

		/*
		* ====================
		*
		* Set the loadr asset to a halted state.
		*
		* @method stop
		* @param p_event_handler {Function} Optional eference to the callback that should fire once any outstanding loads finish loading
		*
		* ====================
		*/

		this.stop = function(p_callback) {
			p_callback = p_callback || null;
			self.halted = true;
			self.haltedObj.callback = p_callback;
		};

	}

	// # # # # # # # # # # # # # # # # # # # # # #

	if(typeof define === 'function' && define.amd) {
		define('loadrAsset', [], function() {
			return loadrAsset;
		});
	}
	global.loadrAsset = loadrAsset;

}(this));