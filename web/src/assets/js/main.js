window.React = require('react/addons'); //make React globally accessible at this point so that each module doesn't need to require it
var Fastclick = require('fastclick'),
  Tracking = require('./tracking.js'),
  api = require('./api.js'),
  mediator = require('./lib/grow/mediator.js'),
  u = require('./lib/grow/util.js');

//set the URL that the API should use
api.setUrl(window.baseApiUrl);

u.init();

//example fastclick, image pre-loading, and api call
$(document).ready(function() {
  Fastclick(document.body); //remove tap delay for mobile devices

  //example asset pre-loading
  var Loader = new loadr();
  Loader.on('completion', function(e) {
      console.log('asset load complete: ', e);
  });
  Loader.add('assets/img/favicon.ico');
  Loader.start();

  //example api call
  api.exampleGet('1', function(result) {
    console.log('result: ', result);
  });

  // example tracking initialization
  Tracking.init();

});

require('./app'); //kick off react rendering / router process
