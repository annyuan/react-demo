/** @jsx React.DOM */

var Router = require('react-router'),
	Route = Router.Route,
	Routes = Router.Routes,
	DefaultRoute = Router.DefaultRoute,
	RouteHandler = Router.RouteHandler,
	Header = require('./ui/header'),
	Home = require('./ui/section/home'),
	Subview1 = require('./ui/section/home/subview1'),
	Subview2 = require('./ui/section/home/subview2'),
	About = require('./ui/section/about'),
	TransitionGroup = require('react/lib/ReactCSSTransitionGroup'),
	mediator = require('./lib/grow/mediator');

var App = React.createClass({

	mixins: [ Router.State ],

	render: function() {
		return (
			<div>
				<TransitionGroup component="section" transitionName="section">
					<RouteHandler key={this.getHandlerKey()}/>
				</TransitionGroup>
			</div>
		);
	},

	

	getHandlerKey: function () {
		// this will all depend on your needs, but here's a typical
		// scenario that's pretty much what the old prop did
		var childDepth = 1; // have to know your depth
		var childName = this.getRoutes()[childDepth].name;
		var id = this.getParams().id;
		var key = childName+id;
		return key;
	},
});

//https://github.com/rackt/react-router
var routes = (
	<Route handler={App}>
		<Route name="home" path="/" handler={Home}>
			<DefaultRoute name="subview1" handler={Subview1} />
			<Route name="subview2" handler={Subview2} />
		</Route>
		<Route name="about" handler={About} />
	</Route>
);

Router.run(routes, Router.HistoryLocation, function (Handler, state) {
	React.render(<Handler/>, document.getElementById('react_content'));
	mediator.publish("ROUTE_CHANGE", state.path);
});