# Events

## <model_name>-update

Dispatchers:
- model objects

Occasion:
- Whenever the data property of a model is edited through the model's set() method.


## changeSectionComplete

Dispatchers:
- router

Occasion:
- Whenver buildPaneConfig() method of the router is called, and a new paneConfig object is created.


## requestAcceptScroll

Dispatchers:
- intro view

Occasion:
- The intro view has animated out and now wants to allow the UI to accept scroll handler callbacks again.


## requestBlockScroll

Dispatchers:
- intro view

Occasion:
- The intro view, which does not want to accept scroll handler callbacks, has animated in.


## requestChangeSection

Dispatchers:
- intro view

Occasion:
- The intro view is animating out and now is able to tell the router how to build the pane config.

Data:
A string identifying the section that needs to be in the rebuilt pane config, i.e. "t shirts".


## requestLoadAssets

Dispatchers:
- product views

Occasion:
- When new product view arrives in the center of the screen

Data:
ID of the image that is in the center of the screen.


## requestPane

Dispatchers:
- intro view

Occasion:
- As the intro view is animating out it asks the scroll handler to auto-advance to the next horizontal pane.

Data:
- A string identifying which way to shift the panes. Can be one of the following:
xBefore
xAfter
yBefore
yAfter


## scroll

Dispatchers:
- scroll_handler

Occasion:
- Every animation frame while the user is moving between panes.

Data:
{
	direction: <x or y>,
	progress: <float between 0 and 1>
}

- If direction is x, the user is scrolling horizontally. If the direction is y, the user is scrolling vertically.
- Progress represents the proportion of the total distance between the current and destination panes that the user has travelled.


## show-screen

Dispatchers:
- delivery view

Occasion:
- User has selected a size on a product listing page

Data:
{
	screen: <screen_id>
}


## splitbuttonclick

Dispatchers:
- split_button view

Occasion:
- user has clicked on the split button

Data:
DOM event object


## switchPanes

Dispatchers:
- scroll_handler

Occasion:
- When the user arrives at a new pane.

Data:
{
	x: <number>,
	y: <number>
}

- This object represents the current position within paneConfig. 


## touchend

Dispatchers:
- user_events

Occasion:
User stops touching the screen

Data:
DOM event object


## touchmove

Dispatchers:
- user_events

Occasion:
User moves finger across the screen

Data:
DOM event object


## touchstart

Dispatchers:
- user_events

Occasion:
- User touches the screen

Data:
DOM event object


## Deploying
Use the deploy tool: https://swag-deploy.grw.io  

- Test: https://swag-web-test.grw.io
- Prod: https://swag-web-prod.grw.io
